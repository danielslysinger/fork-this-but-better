# Intro to Fork Contribution Model

## Forking

Much has been written and rewritten about Git Forks. Much can be read; a lot more can be learned though action.

The short theory of forks is forks are best thought of as a "server-side clone."

## What to expect

These exercises look at forks and will focus on the understanding forks in context of GitLab and Kinsale

1. Creating a fork in GitLab
1. Cloning the fork
1. Configuring the fork in the terminal and IntelliJ
1. Branching in the fork
1. Commits in a fork
1. Contributing "upstream"
1. Syncing your fork

## Workshop

Each workshop exercise is below and will be discussed in the lab.

### Creating a fork in GitLab

This workshop will use a public workshop for per-user level fork isolation on the cloud instance of GitLab.

Navigate to the [Forking Workshop](https://gitlab.com/ki-forking-workshop/fork-this) repository.

Click on the Fork button. 

![img/action-bar.png](img/action-bar.png)

From the dialog, select your account **namespace**.

![namespace.png](img/namespace.png)

The fork will appear in GitLab in your user namespace

![forked.png](img/forked.png)

This a fork. It is a "server-side" clone of the graph data for the original repository. Your fork will become what is 
known in `git` as the `origin`. The original repository, the repository you forked, will be called `upstream`. This will 
be discussed below.

### Cloning the fork

You clone the forked repository like any other repository. Select the `https` URL from the action bar.

![SSH clone](img/ssh-clone.png)

Execute `git clone git@username.gitlab.com:username/fork-this.git` in the Terminal (macOS), `git-bash` 
(Windows), or IntelliJ.

![Clone in IntelliJ](img/intellij-clone.png)

### Configuring the fork in the terminal and IntelliJ

Remember the mention of `origin` above? Every clone of a repository gets an `origin` **remote**. A remote is simply the 
URL of where the clone's network repository exists.

There are many layers of genius built into `git`, and the full offline repository support is one layer. This further means that 
`git` operations have a context of local ("on disk") and network operations. `commit`, `checkout`, `checkout -b` are a 
**sample** of local operations that do not by themselves act on the network. Until you `pull`, `push`, or `fetch`
(among many more) everything exists locally, fully versioned. There are host of operational benefits to this separation 
of local and remote actions, including the ability to craft a commit prior to pushing to a remote for sharing. This 
is somewhat advanced `git`, but easily grokked after practice.

Using `git-bash`, the Terminal, or IntelliJ, let's look at the remote configured on cloning the fork.

`git remote --verbose` (git bash and Terminal)

![Remotes](img/remotes-terminal.png)

In IntelliJ, Git > Manage Remotes... opens the dialog below:

![Remotes](img/remote-intellij.png)

As earlier noted, every clone gets an `origin`.

We must now configure an `upstream` remote. This is **absolutely necessary** to take full advantage of `git`'s graph 
across repositories.

Using the HTTPS URL for the original repository, hereafter referred to as the **upstream** repository, by clicking on the
"Forked from" link on your fork's homepage, see above image. Then copy the HTTPS clone URL, also shown above.

Execute the following in `git-bash`, the Terminal, or IntelliJ's Manage Remotes dialog:

`git remote add upstream https://gitlab.com/ki-forking-workshop/fork-this.git`

In IntelliJ, Git > Manage Remotes... opens the dialogs below:

![Define the new remote](img/define-remote.png)

![Remote added](img/upstream-added.png)

> **HTTPS vs. SSH? What's the difference?**
>
> Working with a fork's `upstream` repository typically means you will be in a READ ONLY relationship. This is another advantage of forks, you don't need to add or manage users in `upstream`. Contributors are providing changes as a `diff` to the `master` branch and the owners or project leaders can review and determine the best time to integrate the change into a release.

> **Fork Always contribution model**
>
> In the "fork always" model, every developer forks the project and contributions are "pulled" or "merged" when work is ready and tests are passing.
>
> Contributions are only accepted on `master` and this forces contributions be continuously up-to-date with `upstream` repositories. It is a highly effective distributed model for CI/CD and promotes "trunk based development."
>
> The Fork Always model scales to large and distributed projects and applications. If you apply for a credit card at Capital One, the Dynamic App Team has a single `master` on `upstream` and all teams fork `upstream`, and team developers fork the team repository. 30+ teams, 120+ developers, contribute to a continuous delivery pipeline on the  `upstream master` that releases to production multiple times a day. No rollbacks.

#### Command Line note

If you have been working in `git-bash` or the Terminal for cloning operations and setting the `upstream remote`, consider
opening the project in IntelliJ and see how these configurations are read by IntelliJ.

### Branching in the fork

You're able, at this point, to do almost anything in this repository. You are an owner. You don't need permissions from 
the `upstream` owner to do anything. You can invite others to this fork. You can branch as much as you want or can 
tolerate. Nothing affects the `upstream` repository. Nothing.

Create a new branch in your fork using the following command:

`git checkout -b attendance`

In IntelliJ, Git > New branch... and checkout the new branch.

**Note** I want everyone to create a branch with the same name. This demonstrates how forks work in isolation and 
foster contributions to the `upstream` repository. Every fork's branches are used in isolation.

### Commits in a fork

Now, let's edit a file and commit the change.

Open `attendance.md`

In the file add your name to the list below mine:

```markdown
# Fork Workshop Attendence

1. Tim Stone, instructor
2. add your name here
```
Wait! Isn't this going to create conflicts? Possibly. A workshop about `git` and especially about forks that didn't 
help you see how to manage an unhappy path would be a bad workshop.

Commit your branch and the changes to your repository. This may look like this in `git-bash` or the Terminal

`git commit -am "Roll call."`

Push the commit and branch to origin, your fork.

`git push -u origin attendance`

IntelliJ's actions are left as an exercise for the IDE user.

### Contributing "upstream"

You contribute to a project `upstream` with GitLab's Merge Request (a.k.a. a Pull Request, but I digress).

Open a Merge Request

When you push a new branch GitLab assists you with helpful action messages, as shown below:

![Create Merge Request](img/create-mr.png)

Click on the button to Create Merge Request.

GitLab is branch and fork "aware," and will default your branch to `master`. You can change this, but for contributions
from a fork, and making some assumptions on a target state for "trunk based development," where the `upstream` repository 
**only** knows about `master`, accept the defaults. 

Edit the title with your name and add a pithy description.

Then "Submit merge request" at the bottom of the page.

### Syncing your fork

As commits land on the `upstream` repository, forks begin to diverge. GitLab lacks visibility on this drift, whereas GitHub will report this drift as commits ahead or behind on the branch, e.g., `master`. 

Keeping a branch up to date is straight forward, but requires some practice and good habits. Let's sync the forks created.

1. Checkout the fork's `master` branch, if not already on `master`

    `git checkout master`

1. Fetch changes. Configuring `upstream` becomes important

    `git fetch --all --prune`

1. Merge the upstream changes from `master`

    `git merge upstream/master`

1. Push to the fork

    `git push origin master`

The fork's SHA on `HEAD` should now match the `upstream` SHA on `HEAD` for `master`

```shell
% git branch -avv
* master                     b16e07d5 [origin/master] Format callouts
  remotes/origin/HEAD        -> origin/master
  remotes/origin/first-draft 1a3a743a Edit README for clarity
  remotes/origin/master      b16e07d5 Format callouts
```
