# Prerequisites

A subtle, but important, use of SSH on Windows and Mac must be addressed prior to the workshop.

## Create a throw-away SSH Key

In your Terminal (macOS) or `git-bash` (Windows) execute the following command:

`ssh-keygen -t rsa -b 2048 -C "workshop ssh key"`

Follow the prompts for the command, noting that you may desire to create a **password-less** key for the 
workshop.

```shell
tstone@TSTONE-DT10 MINGW64 /h
$ ssh-keygen -t rsa -b 2048 -C "workshop ssh key"
Generating public/private rsa key pair.
Enter file in which to save the key (/h//.ssh/id_rsa): /h//.ssh/workshop
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /h//.ssh/workshop
Your public key has been saved in /h//.ssh/workshop.pub
The key fingerprint is:
SHA256:Ym5yofmYGnMBe5mns+6drvbnaaHVNmXOVvQxH1EA314 workshop
The key's randomart image is:
+---[RSA 2048]----+
|            ...o+|
|             ..= |
|  .          ...E|
|   o o      o ..+|
|  . = = S. = .  .|
|   . O oo + +    |
|  o B +o o o     |
|   +.@..o.       |
|  .=B+*+o        |
+----[SHA256]-----+
```

## Add SSH Key to GitLab profile

Copy the value of the `workshop.pub` key and paste it into your profile on GitLab.

![](img/ssh-key-workshop.png)

The Comment will be automatically set, and to provide a sane safety net, set an expiration date on the key.

## Set an SSH Host Alias

Edit your SSH config, found in your home directory at `.ssh/config`.

Add the following:

```shell
Host username.gitlab.com
  Hostname gitlab.com
  User git
  IdentityFile /h/.ssh/workshop
```

This will let GitLab resolve the "alias" to GitLab and use your new key for commits.
